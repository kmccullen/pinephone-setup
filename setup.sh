#!/bin/bash
#######################################################################
#                              Settings                               #
#######################################################################
touch ~/.config/gtk-3.0/settings.ini
echo "[Settings]" >> ~/.config/gtk-3.0/settings.ini
echo "gtk-application-prefer-dark=true" >> ~/.config/gtk-3.0/settings.ini
gsettings set org.gnome.desktop.interface enable-animations false
dconf write /org/gnome/desktop/interface/show-battery-percentage true
dconf write /org/gnome/desktop/interface/clock-show-date true

apt install papirus-icon-theme -y
gsettings set org.gnome.desktop.interface icon-theme 'Papirus'

dconf write /sm/puri/phosh/favorites "['sm.puri.Chatty.desktop', 'org.gnome.Geary.desktop', 'org.gnome.Epiphany.desktop', 'org.gnome.Calls.desktop']"

#######################################################################
#                            Applications                             #
#######################################################################
# setup
apt install flatpak -y
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Git
apt install git -y

# Tmux
apt install tmux -y

# Podcasts
flatpak install https://flathub.org/repo/appstream/org.gnome.Podcasts.flatpakref

# Screenshots
apt install gnome-screenshot -y
scale-to-fit gnome-screenshot
